import java.util.HashMap;
import java.util.Scanner;

public class Palavras {

    /*Crie um sistema que recebe palavras do usuário através do console. O usuário insere
    uma palavra por vez, e ao inserir cada palavra, o sistema mostra uma lista de todas as palavras já inseridas
    e uma contagem de quantas vezes cada palavra foi repetida.


        Exemplo de funcionamento

        Bem vindo!
        Digite uma palavra:
        banana
        A lista de palavras é
        banana 1

        Digite uma palavra:
        maçã
        A lista de palavras é
        banana 1
        maçã 1

    */

    public static void main(String[] args){

        Scanner scann = new Scanner(System.in);

        HashMap<String, Integer> listaPalavras = new HashMap<String, Integer>();
        String palavra;
        String sair = "lol";

//        System.out.rintln(listaPalavras);

        while ( !sair.equals("")){

            int i = 1;

            System.out.println("Digite uma palavra ou 'sair' para finalizar.");
            palavra = scann.nextLine().toLowerCase();

            if (listaPalavras.containsKey(palavra)){
                listaPalavras.put(palavra, ++i);
            } else {
                listaPalavras.put(palavra, i);
            }


            if (palavra.equals("sair")){
                sair = palavra.toUpperCase();
                System.out.println("saiu do programa");
                return;
            }

            for(String item : listaPalavras.keySet()){
                i = listaPalavras.get(item);
                System.out.println(item + " " + i);
            }

        }


    }
}
